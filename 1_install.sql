DELIMITER //

--
-- TABULKY
--

CREATE TABLE counter_type (
  id			INT NOT NULL COMMENT 'Typ počítadla',
  description 		VARCHAR(200) NOT NULL COMMENT 'Popis počítadla',
  PRIMARY KEY (id),
  UNIQUE KEY i1 (description)
) ENGINE=InnoDB COMMENT 'Typy počítadel'
//


CREATE TABLE counter (
  id			INT NOT NULL AUTO_INCREMENT COMMENT 'Id záznamu',
  type_id		INT NOT NULL COMMENT 'Id typu počítadla',
  year 	        	INT(4) NOT NULL COMMENT 'Rok počítadla',
  counter_value 	BIGINT NOT NULL COMMENT 'Hodnota počítadla',
  counter_step	 	INT NOT NULL DEFAULT 1 COMMENT 'Krok počítadla',
  dbchanged             TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Poslední změna hodnoty počítadla',
  PRIMARY KEY (id),
  UNIQUE KEY i1 (type_id, year)
) ENGINE=InnoDB COMMENT 'Hodnoty počítadel'
//
ALTER TABLE counter ADD CONSTRAINT counter_f1 FOREIGN KEY (type_id) REFERENCES counter_type (id) ON DELETE CASCADE
//
 
  
CREATE FUNCTION counter_get
  ( p_type_id INT,
    p_year INT(4) ) RETURNS BIGINT
  MODIFIES SQL DATA
  COMMENT 'Vrací aktuální (první volnou) hodnotu počítadla'
BEGIN
  DECLARE x_counter_value BIGINT;
  DECLARE x_counter_step INT;
  
  SELECT counter_value, counter_step
  INTO   x_counter_value, x_counter_step
  FROM   counter
  WHERE  type_id = p_type_id
  AND    year = p_year
  FOR UPDATE;
  UPDATE counter
  SET    counter_value = x_counter_value + x_counter_step
  WHERE  type_id = p_type_id
  AND    year = p_year;

  RETURN x_counter_value;
END;
//


