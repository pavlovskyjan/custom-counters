--
-- DATA
--

START TRANSACTION;

INSERT INTO counter_type (id, description) VALUES
 (1, 'Dodací listy'),
 (2, 'Faktury'),
 (3, 'Dobropisy');
 
INSERT INTO counter(type_id, year, counter_value) VALUES
 (1, 2015, 1500001),
 (1, 2016, 1600001),
 (1, 2017, 1700001),
 (1, 2018, 1800001),
 (1, 2019, 1900001);
 
 INSERT INTO counter(type_id, year, counter_value) VALUES
 (2, 2015, 1500001),
 (2, 2016, 1600001),
 (2, 2017, 1700001),
 (2, 2018, 1800001),
 (2, 2019, 1900001);
 
 INSERT INTO counter(type_id, year, counter_value) VALUES
 (3, 2015, 1500001),
 (3, 2016, 1600001),
 (3, 2017, 1700001),
 (3, 2018, 1800001),
 (3, 2019, 1900001);

COMMIT; 
