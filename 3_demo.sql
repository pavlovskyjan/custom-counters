DELIMITER //

--
-- DEMO
--

CREATE TABLE invoice (
  id			INT NOT NULL AUTO_INCREMENT COMMENT 'Id faktury',
  num			BIGINT NOT NULL COMMENT 'Číslo faktury',
  date_created    	DATE NOT NULL COMMENT 'Datum vystavení faktury',
  description 		VARCHAR(200) NOT NULL COMMENT 'Dalsí sloupce faktury',
  PRIMARY KEY (id),
  UNIQUE KEY i1 (num)
) ENGINE=InnoDB COMMENT 'Tabulka faktur'
//

CREATE TRIGGER invoice_bi BEFORE INSERT ON invoice
   FOR EACH ROW
BEGIN
   SET NEW.num := counter_get(2, YEAR(NEW.date_created));
END;
//

